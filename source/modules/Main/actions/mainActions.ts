import axios from 'axios';
import { IMainParams } from '../interfaces/IMain';
import { Main } from '../models/additional';

export class MainActions {
    static DEFAULT_URL = '/api';

    /**
     * Тестим получение api
     * @param {*} params
     */
    static test(params: IMainParams): Promise<Main> {
        return axios.get(`${this.DEFAULT_URL}/calculator.php`, { params })
            .then((res: any) => res.data);
    }
}
