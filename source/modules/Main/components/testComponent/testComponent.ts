import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import testComponentTemplate from './testComponentTemplate';

export interface ITestComponentState {
    value: number | null;
}

export interface ITestComponent extends WsReactBaseComponentInterface {
    state: ITestComponentState;

    renameValue(value: number): number;
}

class TestComponent extends WsReactBaseComponent<{}, ITestComponentState> implements ITestComponent {
    state: ITestComponentState = {
        value: 56789,
    }

    /**
     * Изменение value (но не в state)
     * @param value
     */
    renameValue = (value: number): number => {
        return 6789 + value;
    }

    render(): false | JSX.Element {
        return testComponentTemplate(this);
    }
}

export { TestComponent };
