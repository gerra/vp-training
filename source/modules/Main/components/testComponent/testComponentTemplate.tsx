import { FlexBox } from '@thewhite/react-flex-layout';
import { WsReactInput } from '@thewhite/react-input';
import classnames from 'classnames';
import * as React from 'react';
import { ReactSVG } from 'react-svg';
import { ITestComponent } from './testComponent';

const testComponentTemplate = (context: ITestComponent): JSX.Element => {
    return (
        <FlexBox
            column="center center"
            className={classnames(
                'test-component',
            )}
        >
            <FlexBox
                className={classnames(
                    'test-component__wrap',
                )}
            >
                <WsReactInput
                    placeholder="Укажите ключевые слова"
                />
                <FlexBox
                    row="center start"
                    className={classnames(
                        'test-component__content',
                    )}
                >
                    <div
                        className={classnames(
                            'test-component__subject',
                        )}
                    >
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            ЖКХ
                        </FlexBox>
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            Дорожное хозяйство
                        </FlexBox>
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            Экология
                        </FlexBox>
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            Общественнй транспорт
                        </FlexBox>
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            Медицина
                        </FlexBox>
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            Правопорядок
                        </FlexBox>
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            Образование
                        </FlexBox>
                        <FlexBox
                            row="start center"
                            className={classnames(
                                'test-component__subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                'test-component__subject-icon-wrap',
                            )}
                            >
                                <ReactSVG
                                    src="assets/images/svg/subject-icon.svg"
                                    svgClassName={classnames(
                                        'test-component__subject-icon',
                                    )}
                                />
                            </div>
                            Иное
                        </FlexBox>
                    </div>
                    <FlexBox
                        className={classnames(
                            'test-component__category-subject',
                        )}
                    >
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Несанкционированная свалка
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Дворовые территории
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Многоквартирные дома
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Бродячие животные
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Повреждение ограждений
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Неухоженные насаждения
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Неубранные тратуары
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Свисающий снег
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Гололёд на тротуарах
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Висячие сосульки
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Неочищенная дорога
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Неисправность теплоснабжения
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                        <div
                            className={classnames(
                                'test-component__category-subject-item',
                            )}
                        >
                            <div
                                className={classnames(
                                    'ws-body-2-primary',
                                    'test-component__category-subject-item-name',
                                )}
                            >
                                Неисправно освещение
                            </div>
                            <div
                                className={classnames(
                                    'ws-caption-1-secondary',
                                    'test-component__category-subject-item-description',
                                )}
                            >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </div>
                        </div>
                    </FlexBox>
                </FlexBox>
            </FlexBox>
        </FlexBox>
    );
};

export default testComponentTemplate;
