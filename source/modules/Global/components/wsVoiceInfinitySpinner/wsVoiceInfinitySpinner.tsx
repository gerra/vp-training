import classnames from 'classnames';
import * as React from 'react';
import { FlexBox } from 'ws-react-flex-layout';

interface IWsVoiceInfinitySpinnerProps {
    loading: boolean;
    loadingText: string | null;
    loadingShadowBackground: boolean;
    absolute?: boolean;
}

const WsVoiceInfinitySpinner = (props: IWsVoiceInfinitySpinnerProps) => {
    return (
        <React.Fragment>
            {
                !!props.loading &&
                <FlexBox
                    row={'ctr'}
                    className={classnames(
                        'ws-voice-infinity-spinner', {
                            'ws-voice-infinity-spinner--absolute': !!props.absolute,
                            'ws-voice-infinity-spinner--no-back': !props.loadingShadowBackground,
                        },
                    )}
                >
                    <FlexBox
                        column={'start ctr'}
                        className={classnames(
                            'ws-voice-infinity-spinner__content',
                        )}
                    >
                        {
                            !!props.loadingText &&
                            <p
                                className={classnames(
                                    'ws-voice-infinity-spinner__loading-text',
                                    'body-1-primary',
                                )}
                            >
                                {props.loadingText}
                            </p>
                        }
                        <div
                            className={classnames(
                                'ws-voice-infinity-spinner__curve',
                            )}
                        />
                    </FlexBox>
                </FlexBox>
            }
        </React.Fragment>
    );
};

export default WsVoiceInfinitySpinner;
