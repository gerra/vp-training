import del from 'del';
import config from '../config';

function clean(callback) {
    return del(config.DIST_PATH, callback);
}

export { clean };
