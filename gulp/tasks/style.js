import { buildExternalStyles, buildStyles } from '../utils/styleBuild';
import browserSync from 'browser-sync';

function extStyleServe() {
    return buildExternalStyles(true);
}

function extStyle() {
    return buildExternalStyles();
}

function sass() {
    return buildStyles();
}

function stylesReload() {
    return buildStyles()
        .pipe(browserSync.stream());
}

export { extStyleServe, extStyle, sass, stylesReload };
