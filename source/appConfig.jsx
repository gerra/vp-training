import React from 'react';
import { TestComponent } from './modules/Main/components/testComponent/testComponent.ts';

export default function AppConfig() {
    return (
        <div>
            <TestComponent />
        </div>
    );
}
