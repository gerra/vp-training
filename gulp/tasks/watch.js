import gulp from 'gulp';
import { jsLint } from './js';
import { stylesReload } from './style';
import { tsLint } from './ts';
import { php } from './php';

function watch(callback) {
    gulp.watch('source/**/*.scss', gulp.series(stylesReload));
    gulp.watch('source/**/*.{ts,tsx,js,jsx}', gulp.parallel(tsLint, jsLint));
    gulp.watch('source/**/*.php', gulp.series(php));
    return callback();
}

export { watch };
