import { Provider } from 'mobx-react';
import * as React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import NotFound from '../modules/Global/components/notFound/notFoundComponent';
import { MainPageComponent } from '../modules/Main/components/mainPage/mainPageComponent';
import { TestComponent } from '../modules/Main/components/testComponent/testComponent';
import MainWrap from '../modules/Wrappers/mainWrap/mainWrapComponent';
import globalStore from '../store/globalStore';

const stores = { globalStore };

export default class MainRouter extends React.Component<{
    history?: any;
    location?: any;
}> {
    render() {
        return (
            <Provider
                {...stores}
            >
                <HashRouter>
                    <Switch>
                        <Route
                            exact
                            path="/"
                            component={TestComponent}
                        />
                        <MainWrap
                            location={this.props.location}
                        >
                            <Switch>
                                <Route
                                    exact
                                    path="/main-page"
                                    component={MainPageComponent}
                                />
                                <Route
                                    component={NotFound}
                                />
                            </Switch>
                        </MainWrap>
                        <Route
                            component={NotFound}
                        />
                    </Switch>
                </HashRouter>
            </Provider>
        );
    }
}
